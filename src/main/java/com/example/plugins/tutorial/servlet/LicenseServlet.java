package com.example.plugins.tutorial.servlet;

import java.io.IOException;
import javax.servlet.ServletException; 
import javax.servlet.http.HttpServlet; 
import javax.servlet.http.HttpServletRequest; 
import javax.servlet.http.HttpServletResponse;
import com.atlassian.sal.api.ApplicationProperties; 
import com.atlassian.upm.api.license.entity.PluginLicense; 
import com.atlassian.upm.api.util.Option; 
import com.atlassian.upm.license.storage.lib.ThirdPartyPluginLicenseStorageManager;
import org.apache.commons.lang.StringUtils;

public class LicenseServlet extends HttpServlet {
	
	private final ThirdPartyPluginLicenseStorageManager thirdPartyPluginLicenseStorageManager; 
	private final ApplicationProperties applicationProperties;
	
	public LicenseServlet(ThirdPartyPluginLicenseStorageManager thirdPartyPluginLicenseStorageManager, ApplicationProperties applicationProperties)
	{
		this.thirdPartyPluginLicenseStorageManager = thirdPartyPluginLicenseStorageManager; 
		this.applicationProperties = applicationProperties;
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		resp.setContentType("text/html");
		resp.getWriter().write("<html><body>"); 
		outputBody(resp); 
		resp.getWriter().write("</body></html>"); 
		resp.getWriter().close();
	}
	
	private void outputBody(HttpServletResponse resp) throws ServletException, IOException 
	{
		resp.getWriter().write("<br><br>UPM is licensing-aware: " + thirdPartyPluginLicenseStorageManager.isUpmLicensingAware());
		resp.getWriter().write("<br>Plugin key: " + thirdPartyPluginLicenseStorageManager.getPluginKey());
		
		if (!thirdPartyPluginLicenseStorageManager.isUpmLicensingAware()) {
			resp.getWriter().write("<br><br>Update your license");
			resp.getWriter().write("<br><form action=\"" + applicationProperties.getBaseUrl() + "/plugins/servlet/licenseservlet\" method=\"POST\">");
			resp.getWriter().write("<textarea name=\"license\" cols=\"80\" rows=\"10\">"); 
			for (String storedRawLicense : thirdPartyPluginLicenseStorageManager.getRawLicense()) {
				//enter the stored license into the textarea, if the license has been stored 
				resp.getWriter().write(storedRawLicense);
			} 
			resp.getWriter().write("</textarea>"); resp.getWriter().write("<br><input type=\"submit\" value=\"Save\" />"); resp.getWriter().write("</form>");
		} else {
			resp.getWriter().write("<br>Cannot modify plugin licenses with this API when UPM is licensing-aware. Please use ");
			resp.getWriter().write("<a href=\"" + thirdPartyPluginLicenseStorageManager.getPluginManagementUri() + "\">UPM's licensing UI</a>.");
		}
	}
	
	@Override protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
	IOException {
		
		resp.setContentType("text/html"); 
		resp.getWriter().write("<html><body><b>");
		
		if (!thirdPartyPluginLicenseStorageManager.isUpmLicensingAware()) 
		{
			String license = req.getParameter("license"); 
			if (!StringUtils.isEmpty(license)) 
			{
				//we have a non-empty license parameter - let's update the license if it is valid.
				Option<PluginLicense> validatedLicense = thirdPartyPluginLicenseStorageManager.validateLicense(license);
				
				if (validatedLicense.isDefined()) 
				{
					thirdPartyPluginLicenseStorageManager.setRawLicense(license); 
					resp.getWriter().write("Valid license. License has been updated.");
				} 
				else 
				{
					resp.getWriter().write("Invalid license. License has not been updated.");
				}
			} 
			else 
			{
				//we have an empty/null license parameter - let's remove the stored license 
				thirdPartyPluginLicenseStorageManager.removeRawLicense(); 
				resp.getWriter().write("License has been removed.");
			}
		} 
		else 
		{
			//don't allow POSTs to occur to our license servlet if UPM is licensing-aware
			resp.getWriter().write("Nice try! You cannot update your plugin license with this API when UPM is licensing-aware.");
		}
		
		resp.getWriter().write("</b>"); 
		outputBody(resp); 
		resp.getWriter().write("</body></html>"); 
		resp.getWriter().close();	
	}
}